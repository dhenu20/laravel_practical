<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
          $data['title'] = 'Dashboard';
        $data['routes'] = "dashboard";
        $data['pagetitle'] = 'Dashboard | Laravel Practical';
        return view('admin/dashboard',$data);
    }
    public function adminHome()
    {
        $data['title'] = 'Dashboard';
        $data['routes'] = "dashboard";
        $data['pagetitle'] = 'Dashboard | Laravel Practical';
        return view('adminHome',$data);
    }
}
