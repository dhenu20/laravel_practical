
 @include('admin.layouts.stylesheet')


  <!-- Navbar -->
   @include('admin.layouts.navbar')
  
  <!-- /.navbar -->

  <!-- Main Sidebar Container -->
 @include('admin.layouts.mainsidebar')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard</div>
                <div class="card-body">
                    You are Admin.
                </div>
            </div>
        </div>
    </div>
</div>
@include('admin.layouts.footer')

  
<!-- ./wrapper -->

<!-- jQuery -->
@include('admin.layouts.scripts')
