<aside class="main-sidebar sidebar-dark-primary elevation-4">
   <!-- Brand Logo -->
   <a href="dashboard" class="brand-link">
  <img src="{{asset('uploads/logo.png')}}" alt=" Logo" class="brand-image img-circle elevation-3" style="opacity: .8"> 
   <span class="brand-text font-weight-light text-center">Delivery App</span>
   </a>
   <?php 
  $currentPath= Route::getFacadeRoot()->current()->uri();
  $root = explode('/',$currentPath);
  $root = $root[0];
 ?>
   <!-- Sidebar -->
   <div class="sidebar">
      <!-- Sidebar user panel (optional) -->
      <!-- Sidebar Menu -->
      <nav class="mt-2">
         <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
            <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
            <li class="nav-item has-treeview menu-open">
               <a href="dashboard" class="nav-link  <?php if($root == 'dashboard'){ echo 'active'; } ?>">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Dashboard</p>
               </a>
            </li>
            <!-- <li class="nav-header pt-1">EXAMPLES</li> -->
           
     <!--  // $role = Session::get('role'); -->
     </ul>
      </nav>
      <!-- /.sidebar-menu -->
   </div>
   <!-- /.sidebar -->
</aside>
