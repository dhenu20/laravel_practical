<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\UserController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::resource('dashboard', HomeController::class);




Auth::routes();

Route::get('/dashboard', [App\Http\Controllers\HomeController::class, 'index'])->name('dashboard');

Route::get('users',[App\Http\Controllers\UserController::class, 'show'])->name('users');
Route::get('adduser',[App\Http\Controllers\UserController::class, 'create'])->name('adduser');
Route::post('add-User',[App\Http\Controllers\UserController::class, 'store'])->name('add-User');
Route::get('edituser/{id}',[App\Http\Controllers\UserController::class, 'edit'])->name('edituser');
Route::post('update_user/{id}',[App\Http\Controllers\UserController::class, 'update'])->name('update_user');
Route::delete('deleteuser/{id}',[App\Http\Controllers\UserController::class, 'destroy'])->name('deleteuser');
Route::get('profile-update/{id}',[App\Http\Controllers\UserController::class, 'destroy'])->name('profile-update');

